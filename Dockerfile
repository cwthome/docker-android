# based on https://github.com/mreichelt/docker-android
FROM beevelop/java

MAINTAINER Andrew Leech <andrew@alelec.net>

ENV ANDROID_SDK_URL="https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip" \
    ANT_HOME="/usr/share/ant" \
    MAVEN_HOME="/usr/share/maven" \
    GRADLE_HOME="/usr/share/gradle" \
    ANDROID_HOME="/opt/android"

ENV PATH $PATH:$ANDROID_HOME/tools:$ANDROID_HOME/tools/bin:$ANDROID_HOME/platform-tools:$ANDROID_HOME/build-tools/$ANDROID_BUILD_TOOLS_VERSION:$ANT_HOME/bin:$MAVEN_HOME/bin:$GRADLE_HOME/bin

WORKDIR /opt

RUN dpkg --add-architecture i386 && \
    apt-get -qq update && \
    apt-get -qq install -y wget curl maven ant git gradle libncurses5:i386 \
    libstdc++6:i386 zlib1g:i386 android-sdk-build-tools android-tools-fsutils \
    file libpulse0 qt5-default git dos2unix sudo gawk coreutils cpio zip \
    nodejs npm nodejs-legacy software-properties-common

# Install python 3.6 & 3.7
RUN add-apt-repository --yes ppa:deadsnakes/ppa && \
    apt-get -qq update && \
    apt-get -qq install -y python3.6 python3.7

# Installs Android SDK
RUN mkdir android && cd android && \
    wget -O tools.zip ${ANDROID_SDK_URL} && \
    unzip tools.zip && rm tools.zip
    
# get more from `sdkmanager --list` (add '--verbose' to read long package names)
RUN yes | sdkmanager --verbose \
      'tools' \
      'platform-tools' \
      'build-tools;25.0.1' \
      'build-tools;25.0.2' \
      'build-tools;25.0.3' \
      'build-tools;26.0.0' \
      'build-tools;26.0.1' \
      'build-tools;26.0.2' \
      'build-tools;26.0.3' \
      'build-tools;27.0.0' \
      'build-tools;27.0.1' \
      'build-tools;27.0.2' \
      'build-tools;27.0.3' \
      'build-tools;28.0.3' \
      'platforms;android-25' \
      'platforms;android-26' \
      'platforms;android-27' \
      'platforms;android-28' \
      'extras;android;m2repository' \
      'extras;google;m2repository' \
      'extras;google;google_play_services' \
      && \
    chmod a+x -R $ANDROID_HOME && \
    chown -R root:root $ANDROID_HOME

# Install Cordova
RUN npm install -g cordova

# Install git-lfs
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash && \
    apt-get install git-lfs